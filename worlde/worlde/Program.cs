﻿using System;
using System.IO;
using System.Collections.Generic;

class Wordle
{
    static void Main()
    {
        var start = new Wordle();
        start.StartingProgram();
    }

    //Archivo ejemplo  Tipo_Idioma_selectedLang.txt        Menu_esp_selectedLang.txt
    // Menu_  Hub_   esp_   selectedLang.txt
    // Title_en_selectedLang.txt
    //Carpeta ejemplo: \Hub || \GetLanguage || \xD

    public void StartingProgram()
    {
        Directory.SetCurrentDirectory(@"..\..\..\..\Wordle");
        //CheckFiles();
        string worldeLanguage = GetLanguage(); //Pedimos un idioma para empezar a ejecutar el juego

        Hub(worldeLanguage);
    }

    public void CheckFiles()
    {
        //Funcion que Comprueba existencia, 
    }


    public void Hub(string language)
    {
        bool menu = true;
        bool exit = false;
        string hubFile = "Hub_"; //String Specific para la funcion Print



        string name = null;
        do //Bucle para el nombre
        {
            int i = 0; //Contador para mostrar el error si no introduce nada el Usuario

            Console.ForegroundColor = ConsoleColor.Red;
            if (i > 0) Console.WriteLine($"Error. Introduced name is not valid"); //Mensaje de error en caso de que el usuario no introduzca nombre
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Player name: ");
            name = Console.ReadLine().Replace(" ", ""); //Pedimos nombre de usuario

            i++;
        } while (name == null);

        do //Bucle de menú
        {
            PrintInterface(hubFile, language, @"Hub\", menu); //printamos interfaz del Hub
            char option = Console.ReadKey().KeyChar;
            switch (option)
            {
                case '1':
                    PlayWordle(RandomWord(language), language, name); //Extrae de manera aleatoria una palabra del archivo
                    break;
                case '2':
                    language = GetLanguage(); //Permite volver a cambiar el idioma
                    break;
                case '3':
                    Historial(false, 0, " ", " "); //Funcion que muestra el historial de partidas
                    break;
                case '4':
                    exit = true;
                    break;

            }
        } while (!exit);
    }


    public void Historial(bool victory, int score, string word, string name)
    {
        Console.Clear();
        if (victory == true) //En caso de victoria registramos la partida
        {
            string record = $@"{name} get the word: {word} at {score} try";

            StreamWriter sw = new StreamWriter("Historial.txt");
            sw = File.AppendText("Historial.txt");
            sw.WriteLine(record);
            sw.Close();
        }

        string[] records = File.ReadAllLines("Historial.txt");
        foreach (string lanes in records) //Mostramos el historial de partidas
        {
            Console.WriteLine(lanes);
        }
        Console.ReadKey();
    }


    public string RandomWord(string language)
    {

        string wordsText = File.ReadAllText(language);
        string[] words = wordsText.Split(',');
        Random rnd = new Random();
        return words[rnd.Next(words.Length)];
    }


    public void PlayWordle(string word, string language, string name)
    {
        int tries = 7;
        bool victory = false;

        do
        {
            PrintInterface("Title_", language, @"PlayWordle\", false);

            Console.WriteLine($"\n-{tries}-  -WLength{word.Length}-");

            string usrword = GetUserWord(word, ref tries);

            PrintWord(CheckWord(usrword, word), usrword, 100);

        } while (tries != 0 || !victory);

        Historial(victory, tries, word, name);
    }


    public int[] CheckWord(string userWord, string word)
    {

        int[] correctWord = new int[userWord.Length];
        for (int i = 0; i < word.Length; i++)
        {
            for (int j = 0; j < userWord.Length; j++)
            {
                if (userWord[j] == word[i])
                {
                    correctWord[j] = 2; //La letra esta en el lugar equivocado
                }
            }

            if (userWord[i] == word[i])
            {
                correctWord[i] = 1; //La letra esta en el lugar correcto
            }
            else correctWord[i] = 0; //La letra no esta en la palabra
        }
        return correctWord;
    }


    public void PrintWord(int[] wordPos, string usrWord, int textspeed)
    {
        for (int i = 0; i < wordPos.Length; i++)
        {
            if (wordPos[i] == 1) Console.BackgroundColor = ConsoleColor.Green;
            if (wordPos[i] == 2) Console.BackgroundColor = ConsoleColor.DarkYellow;
            if (wordPos[i] == 0) Console.BackgroundColor = ConsoleColor.Red;


            Console.Write(usrWord[i]);
            Thread.Sleep(textspeed); //Funcion que relentiza la escritura, me hizo gracia y la puse :^)
            Console.BackgroundColor = ConsoleColor.Black;
        }
        Console.ReadKey();
    }


    public string GetUserWord(string word, ref int tries)
    {
        string userWord = "";

        //Podria haber escrito a mano el error, pero a veces hago estas cosas cuando me olvido momentaneamente de algo,
        //ya que soy olvidadizo a ver si se me queda xD y como aun no se valora optimización me ayuda >:), espero que no sea molestia ^^
        StreamReader reader = new StreamReader("Errors.txt");
        string errorMsg = reader.ReadToEnd();
        string[] error = errorMsg.Split("\n");
        reader.Close();
        
        do
        {
            Console.WriteLine("______________________________________");
            userWord = Console.ReadLine().ToLower().Replace(" ", "");

            if (userWord.Length != word.Length) Console.WriteLine(error[1] + $" Actual word Length = {word.Length}");
        } while (userWord.Length != word.Length && userWord != "" && userWord != null);

        return userWord;
    }


    public string GetLanguage()
    {
        string selectedLang = null;

        do
        {
            Console.Clear();
            Console.WriteLine("\t\tSelect your language");

            Console.WriteLine("\n\t===========================");
            Console.WriteLine("\t     1.-       Català      ");
            Console.WriteLine("\t     2.-       Español     ");
            Console.WriteLine("\t     3.-       English     \n");
            Console.WriteLine("\t===========================");

            char select = Console.ReadKey().KeyChar;
            switch (select)
            {
                case '1':
                    selectedLang = "cat_selectedLang.txt";
                    break;
                case '2':
                    selectedLang = "esp_selectedLang.txt";
                    break;
                case '3':
                    selectedLang = "en_selectedLang.txt";
                    break;
                default:
                    Console.WriteLine("Seleccione un idioma existente");
                    Console.ReadKey();
                    break;
            }
        } while (selectedLang == null);
        Console.Clear();
        return selectedLang;
    }


    //string especific añade detalle al nombre del archivo, si es un menú, si es un dibujo,etc  Menu_... || Title_...
    //string lang nos da el idioma del archivo Menu_esp_
    //El Path nos permite saber si es del \Hub, si es de \PlayWordle o cualquier cosa

    //No creo que esta sea la forma mas correcta de hacer esto,
    //pero quería probar a hacer como una especie de "sistema" para acceder a diversos archivos con las herramientas que tengo ahora mismo
    public void PrintInterface(string especific, string lang, string path, bool menu)
    {
        Console.Clear();

        //Printamos la interfaz que queramos
        //Añadimos a path el archivo con especific y lang para determinar que queremos printar

        lang = especific + lang;

        StreamReader getInterface = new StreamReader(path + lang);
        Console.WriteLine(getInterface.ReadToEnd());
        getInterface.Close();

        if (menu)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.Gray;

            //Le añadimos que es un menu
            lang = "Menu_" + lang;

            //Cogemos la interfaz del menú
            StreamReader getMenuInterface = new StreamReader(path + lang);
            Console.WriteLine(getMenuInterface.ReadToEnd());
            getInterface.Close();
        }
        Console.ForegroundColor = ConsoleColor.White;
        Console.BackgroundColor = ConsoleColor.Black;
    }
}